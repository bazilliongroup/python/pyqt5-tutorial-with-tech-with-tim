######################################################################################################
# @file        tutorial3.py
# @brief       PyQt5 Tutorial - How to Use Qt Designer
##########################################################
#
# Resources :https://www.youtube.com/watch?v=FVpho_UiDAY&list=PLzMcBGfZo4-lB8MZfHPLTEHO9zJDDLpYj&index=3
#             C:\Users\RonB\venv\Lib\site-packages - The site Packages folder
#             C:\Users\RonB\venv\Lib\site-packages\QtDesigner - QT Designer (venv is the virtual environment)
#             pyuic5 -x tutorial3Ui.ui -o tutorial3Ui.py --command to turn Ui file to python code.
#
#
#
# Modifications:   10/11/2020   R. Bazillion    First Draft
#
######################################################################################################