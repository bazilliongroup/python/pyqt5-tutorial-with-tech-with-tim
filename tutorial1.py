######################################################################################################
# @file        tutorial1.py
# @brief       PyQt5 Tutorial - Setup and a Basic GUI Application
##########################################################
#
# Resources : https://www.youtube.com/watch?v=Vde5SH8e1OQ&list=PLzMcBGfZo4-lB8MZfHPLTEHO9zJDDLpYj&index=1
#
# Modifications:   10/11/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys


def window():
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(200, 200, 300, 300)
    win.setWindowTitle("Ronald Bazillion")

    label = QtWidgets.QLabel(win)
    label.setText("my first label!")
    label.move(50, 50)

    win.show()
    sys.exit(app.exec_())


window()
