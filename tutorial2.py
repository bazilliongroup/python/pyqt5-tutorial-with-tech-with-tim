######################################################################################################
# @file        tutorial2.py
# @brief       PyQt5 Tutorial - Buttons and Events (Signals)
##########################################################
#
# Resources : https://www.youtube.com/watch?v=-2uyzAqefyE&list=PLzMcBGfZo4-lB8MZfHPLTEHO9zJDDLpYj&index=2
#
# Modifications:   10/11/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.b1 = QtWidgets.QPushButton(self)       # create button - The self is the inherited QMainWindow
        self.label = QtWidgets.QLabel(self)         # create label - The self is the inherited QMainWindow
        self.setGeometry(200, 200, 300, 300)
        self.setWindowTitle("Otto Excellence")
        self.initUi()

    def initUi(self):
        self.label.setText("my first label!")
        self.label.move(50, 50)

        self.b1.setText("Click me")
        self.b1.clicked.connect(self.clicked)

    def clicked(self):
        self.label.setText("you pressed the button")
        self.update()

    def update(self):
        self.label.adjustSize()


def window():
    app = QApplication(sys.argv)
    win = MyWindow()
    win.show()
    sys.exit(app.exec_())


window()
